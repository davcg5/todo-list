package com.example.davidc.todolist

import android.support.v7.app.AppCompatActivity
import com.google.firebase.database.FirebaseDatabase
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import java.util.*

class ListDetailActivity : AppCompatActivity(){
    val database = FirebaseDatabase.getInstance()
    val ref = database.getReference("todo-list")
    internal lateinit var addButton: Button
    internal lateinit var  nameOfList : String

    lateinit var listRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?){
super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_list_detail)
        val listId = intent.getStringExtra(MainActivity.INTENT_LIST_ID);
        val ref2 = database.getReference("todo-list/"+listId+"/Pendientes");
        ref.child(listId).child("list-name")
                .addListenerForSingleValueEvent(object:ValueEventListener{
                    override fun onCancelled(p0: DatabaseError){

                    }
                    override fun onDataChange(dataSnapshot: DataSnapshot){
                        title = dataSnapshot.value.toString()
                    }
                })




        listRecyclerView= findViewById(R.id.idRecyclerView)
        listRecyclerView.layoutManager= LinearLayoutManager(this)
        listRecyclerView.adapter= ListDetailRecyclerViewAdapter(this,ref2)



        nameOfList = listId


        addButton = findViewById(R.id.addButton)

        addButton.setOnClickListener{
            _ -> showCreateDetail()
        }

    }

    private fun showCreateDetail(){
        val dialogTitle = "Add To Do"
        val positiveButtonTitle = "Add"

        val builder = AlertDialog.Builder(this)
        val listTitleEditText = EditText(this)
        listTitleEditText.inputType = InputType.TYPE_CLASS_TEXT


        builder.setTitle(dialogTitle)
        builder.setView(listTitleEditText)

        builder.setPositiveButton(positiveButtonTitle){
            dialog, i ->
            val newToDo = listTitleEditText.text.toString()

            val newId = UUID.randomUUID().toString()

            ref.child(nameOfList).
                    child("Pendientes").child(newId).child("taskName").
                    setValue(newToDo)


            dialog.dismiss();

            dialog.dismiss()
        }

        builder.create().show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }



}