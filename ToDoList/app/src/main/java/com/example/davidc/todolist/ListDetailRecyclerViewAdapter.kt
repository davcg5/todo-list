package com.example.davidc.todolist

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError


class ListDetailRecyclerViewAdapter(context: Context ,ref : DatabaseReference):
        RecyclerView.Adapter<ListSelectionRecyclerViewHolder>(){

    val toDoLists:ArrayList<TodoList> = arrayListOf()
    init{

        ref.addChildEventListener(object : ChildEventListener{
            override fun onCancelled(item: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onChildMoved(item: DataSnapshot, p1: String?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onChildChanged(item: DataSnapshot, p1: String?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
            override fun onChildAdded(item: DataSnapshot, p1: String?) {
                val listTitle = item.child("taskName").value.toString();
                val listId = item.key.toString();
                toDoLists.add(TodoList(listId, listTitle));
                //notifyDataSetChanged();
                notifyItemInserted(toDoLists.size)

            }

            override fun onChildRemoved(item: DataSnapshot) {
                val deletedIndex =      toDoLists.indexOfFirst{ element -> element.id == item.key}

                toDoLists.removeAt(deletedIndex)
                notifyItemRemoved(deletedIndex)
            }

        })
    }


    override fun onCreateViewHolder
            (parent: ViewGroup,
             viewType: Int): ListSelectionRecyclerViewHolder {


        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_selection_view_holder, parent, false )
        return ListSelectionRecyclerViewHolder(view)
    }

    override fun getItemCount(): Int {
        return toDoLists.count()

    }

    override fun onBindViewHolder(holder: ListSelectionRecyclerViewHolder, position: Int) {
        holder.listTitle.text=toDoLists[position].listName

        holder.listTitle.text =  toDoLists[position].listName




    }

}
