package com.example.davidc.todolist

import android.os.Bundle
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import com.google.firebase.database.FirebaseDatabase


import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), ListSelectionRecyclerViewAdapter.ListSelectionRecyclerviewClicListener {

    lateinit var listRecyclerView: RecyclerView
    val database = FirebaseDatabase.getInstance()
    val ref = database.getReference("todo-list" )

    companion object {
        val INTENT_LIST_ID = "listId"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        listRecyclerView= findViewById(R.id.idRecyclerView)
        listRecyclerView.layoutManager= LinearLayoutManager(this)
        listRecyclerView.adapter= ListSelectionRecyclerViewAdapter(ref, this)

        fab.setOnClickListener { view ->
            showCreasteListDialog()
        }

    }

    private fun showCreasteListDialog(){
        val dialogTitle= getString(R.string.name_of_list);
        val positiveButtonTitle= getString(R.string.creat_list);

        val builder = AlertDialog.Builder(this);
        val listTitleEditText = EditText(this);
        listTitleEditText.inputType= InputType.TYPE_CLASS_TEXT;

        builder.setTitle(dialogTitle);
        builder.setView(listTitleEditText);

        builder.setPositiveButton(positiveButtonTitle){dialog, i ->
            val newList = listTitleEditText.text.toString()
            ref.child(UUID.randomUUID().toString()).
                    child("list-name").
                    setValue(newList)

            dialog.dismiss();
        }


        builder.create().show();
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
    private fun showListDetail(listId : String){
        val listDetailIntent = Intent(this, ListDetailActivity :: class.java)
        //Para pasar info a otro activity
        //parse Label si necesitamos enviar un objeto completo, con datos primitivos no problem

        listDetailIntent.putExtra(INTENT_LIST_ID,listId)


        startActivity(listDetailIntent)

    }

    //Legado: Tu manejas el Clic yo no
    override fun listenerItemClicked(todoList: TodoList) {
        showListDetail(todoList.id)
    }

}
